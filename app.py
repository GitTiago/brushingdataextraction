from codecs import getreader
from datetime import date
from typing import List, Dict, Optional

import uvicorn
from fastapi import FastAPI, Request, File, UploadFile
from fastapi import status
from fastapi.responses import HTMLResponse
from fastapi.responses import Response, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from brushing import Brusher, collect_user_brushing_data, collect_group_user_brushing_data, obtain_week

fleeting_user_data: Optional[Dict[str, List[Brusher]]] = None


def setup_fleeting_user_data():
    global fleeting_user_data
    if not fleeting_user_data:
        fleeting_user_data = dict()


def destroy_fleeting_user_data():
    global fleeting_user_data
    if fleeting_user_data:
        del fleeting_user_data


templates = Jinja2Templates(directory="templates")


app = FastAPI(
    title="Brushing data",
    on_startup=[setup_fleeting_user_data],
    on_shutdown=[destroy_fleeting_user_data]
)
app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/")
def root():
    return RedirectResponse("/brushings/submit/")


@app.get("/brushings/submit/",  response_class=HTMLResponse)
def get_submit_page(request: Request):
    return templates.TemplateResponse("csv_submit.jinja2", {"request": request})


@app.post("/brushings/submit/", response_class=RedirectResponse)
def create_upload_files(files: List[UploadFile] = File(...)):
    global fleeting_user_data
    group_assignment_files = [file for file in files if "group" in file.filename]
    raw_data_files = [file for file in files if "rawdata" in file.filename]
    user_brushing_data = dict()

    StreamReader = getreader('utf-8')

    for raw_data_file in raw_data_files:
        with StreamReader(raw_data_file.file) as raw_data_file_text:
            user_brushing_data.update(collect_user_brushing_data(raw_data_file_text))

    for group_assignment_file in group_assignment_files:
        with StreamReader(group_assignment_file.file) as group_assignment_text_file:
            group_user_brushing_data = collect_group_user_brushing_data(user_brushing_data, group_assignment_text_file)
            for group, brushers in group_user_brushing_data.items():
                for brusher in brushers:
                    brusher.process()

            fleeting_user_data.update(group_user_brushing_data)
    return RedirectResponse("/brushings/submit/", status_code=status.HTTP_303_SEE_OTHER)


@app.get("/brushing/data/{day}", response_class=HTMLResponse)
def display_user_data(request: Request, day: date):
    week = obtain_week(day)
    global fleeting_user_data

    return templates.TemplateResponse(name="user_data.jinja2",
                                      context={"request": request,
                                               "user_data": fleeting_user_data,
                                               "week": week
                                               })


@app.get("/brushing/user/{user_id}/{day}", response_class=HTMLResponse)
def get_user_data(request: Request, user_id: str, day: date):
    week = obtain_week(day)
    global fleeting_user_data
    for group, users in fleeting_user_data.items():
        for user in users:
            if user.playbrush_id == user_id:
                return templates.TemplateResponse(name="user_data.jinja2",
                                                  context={"request": request,
                                                           "user_data": {group: [user]},
                                                           "week": week
                                                           })

    return Response(status_code=status.HTTP_404_NOT_FOUND)


if __name__ == '__main__':
    uvicorn.run(app)
