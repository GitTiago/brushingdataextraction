import csv
import locale
from collections import defaultdict
from datetime import datetime, date, timedelta
from decimal import Decimal
from functools import reduce
from itertools import chain, count
from typing import NamedTuple, Dict, Optional, List, Tuple, TextIO

TIMESTAMP_FORMAT = '%a %b %d %Y %H:%M:%S %Z%z'


class BrushingSession(NamedTuple):
    """
    An object representing the brushing session. Having the start and finish time for the session as well as the time
    spent brushing categorized by direction: Up, Down, Left, Right, Other.
    """
    timestamp: datetime

    up_seconds: Decimal
    down_seconds: Decimal
    left_seconds: Decimal
    right_seconds: Decimal
    other_seconds: Decimal

    @property
    def total_seconds(self) -> Decimal:
        """
        Returns the cumulative time spent brushing in the session, including all directions and unspecified time.
        :return:
        """
        return sum((self.up_seconds, self.down_seconds, self.left_seconds, self.right_seconds, self.other_seconds))

    @property
    def finish_timestamp(self) -> datetime:
        """
        Returns the timestamp for when the brushing session finished by summing the starting timestamp with the
        cumulative time spent brushing.
        :return: A datetime representing when the brushing session finished.
        """
        return self.timestamp + timedelta(seconds=float(self.total_seconds))

    def __add__(self, other):
        if not isinstance(other, BrushingSession):
            return NotImplemented
        return BrushingSession(
            timestamp=min(self.timestamp, other.timestamp),
            up_seconds=self.up_seconds + other.up_seconds,
            down_seconds=self.down_seconds + other.down_seconds,
            right_seconds=self.right_seconds + other.right_seconds,
            left_seconds=self.left_seconds + other.left_seconds,
            other_seconds=self.other_seconds + other.other_seconds
        )


class ProcessedDayBrushingReport:
    """
    This class represents the valid brushing sessions done in a day. By instancing it with multiple `BrushingSession`
    for the same day it will process them into two significant brushing sessions, `morning_brush` and `evening_brush`.
    It will merge any sessions that are separated by less than two minutes. After they are merged it will pick
    the longest session for both morning and evening.
    Merged sessions with a duration under 20 seconds are not considered.
    """
    def __init__(self, raw_brushing_sessions: List[BrushingSession]):
        self.raw_brushing_sessions = list(raw_brushing_sessions)
        self.morning_brush, self.evening_brush = self._process_raw_report()

    @property
    def valid_brushes(self) -> List[BrushingSession]:
        """
        Returns a list with the defined sessions.
        :return: A list with the morning and evening brushing session. Length will vary from 0 to 2 depending on whether
        these are defined.
        """
        return [session for session in (self.morning_brush, self.evening_brush) if session is not None]

    @property
    def n_valid_brushes(self) -> int:
        return len(self.valid_brushes)

    def _process_raw_report(self) -> Tuple[Optional[BrushingSession], Optional[BrushingSession]]:
        raw_brushing_sessions = self.raw_brushing_sessions
        # Caution, this alters the original order
        raw_brushing_sessions.sort(key=lambda x: x.timestamp)

        merge_groups: List[List[BrushingSession]] = list()

        current_merge_group_index = 0
        if raw_brushing_sessions:
            merge_groups.append(list())
            merge_groups[0].append(raw_brushing_sessions[0])
        for first_session, second_session, index in zip(raw_brushing_sessions, raw_brushing_sessions[1:], count(1)):
            if (second_session.timestamp - first_session.finish_timestamp) > timedelta(minutes=2):
                current_merge_group_index += 1
                merge_groups.append(list())
            merge_groups[current_merge_group_index].append(second_session)

        merged_brushes = list()
        for merge_group in merge_groups:
            session = reduce(lambda x, y: x+y, [brush_session for brush_session in merge_group])
            merged_brushes.append(session)

        morning_brush: Optional[BrushingSession] = None
        evening_brush: Optional[BrushingSession] = None

        for brush in merged_brushes:
            if brush.total_seconds < 20:
                continue
            two_pm_timestamp = brush.timestamp.replace(hour=14, minute=0, second=0, microsecond=0)
            is_evening_brush = brush.timestamp > two_pm_timestamp
            if is_evening_brush:
                if evening_brush is None or evening_brush.total_seconds < brush.total_seconds:
                    evening_brush = brush
            else:
                if morning_brush is None or morning_brush.total_seconds < brush.total_seconds:
                    morning_brush = brush

        return morning_brush, evening_brush


class Brusher:
    """
    A representation of a brush user and all the data pertaining to them.
    You may add any number of `BrushingSession` with `add_raw_session` to it before calling `process`.
    Processing will create a `ProcessedDayBrushingReport` for each day that there is a `BrushingSession` in
     and allows for the use of the properties:
     * `total_brushes`
     * `total_twice_brushes`
     * `all_processed_brushes`
     * `average_brushing_time`
    As well as the methods:
     * get_day_sessions
     * n_day_sessions

    If you add any raw data you should call process again to update the data.
    """
    def __init__(self, playbrush_id: str):
        self.day_to_raw_brushes_mapping: Dict[date, List[BrushingSession]] = defaultdict(list)
        self.playbrush_id = playbrush_id
        self.day_to_processed_brushes_report_mapping: Optional[Dict[date, ProcessedDayBrushingReport]] = None

    def add_raw_session(self, brushing_session: BrushingSession):
        """
        Adds a session to the Brusher, it will be considered in `process`.
        :param brushing_session: A session to be considered in the processing that will be done.
        :return:
        """
        session_date = brushing_session.timestamp.date()
        self.day_to_raw_brushes_mapping[session_date].append(brushing_session)

    @property
    def total_brushes(self) -> int:
        """
        Returns the number of valid brushing sessions after processing.
        :return:
        """
        return len(self.all_processed_brushes)

    @property
    def total_twice_brushes(self):
        """
        Returns the number of days that this user brushed this teeth twice in the same day.
        :return:
        """
        return sum(
            (
                1 for day_brushing_report
                in self.day_to_processed_brushes_report_mapping.values()
                if len(day_brushing_report.valid_brushes) == 2
            )
        )

    @property
    def all_processed_brushes(self) -> List[BrushingSession]:
        """
        Returns all the brushes this user has registered against them.
        :return:
        """
        return list(chain(*(day_brushing_report.valid_brushes for day_brushing_report
                            in self.day_to_processed_brushes_report_mapping.values())))

    @property
    def average_brushing_time(self) -> Decimal:
        """
        Returns how much time this user spent on each session on average.
        :return:
        """
        all_brushes = self.all_processed_brushes
        total_brushes = len(all_brushes)
        if total_brushes == 0:
            return Decimal()
        return sum((brush.total_seconds for brush in all_brushes)) / Decimal(str(total_brushes))

    def get_day_sessions(self, date_: date) -> List[BrushingSession]:
        """
        Gets all the BrushingSession (max 2., morning and evening) that the user did in a specified date.
        :param date_:
        :return:
        """
        report = self.day_to_processed_brushes_report_mapping.get(date_)
        if report is None:
            return []
        else:
            return report.valid_brushes

    def n_day_sessions(self, date_: date) -> int:
        """
        Returns the number of BrushingSession a user did in a specified date.
        :param date_:
        :return:
        """
        return len(self.get_day_sessions(date_))

    def process(self):
        """
        Creates ProcessedDayBrushingReport for each relevant day stored in day_to_raw_brushes_mapping
        and stores it as self.day_to_processed_brushes_report_mapping.
        :return:
        """
        self.day_to_processed_brushes_report_mapping = dict()
        for day, raw_brushing_report in self.day_to_raw_brushes_mapping.items():
            self.day_to_processed_brushes_report_mapping[day] = ProcessedDayBrushingReport(raw_brushing_report)


def collect_user_brushing_data(csv_file_content: TextIO) -> Dict[str, Brusher]:
    """
    Creates a dictionary from a csv file with user ids for its keys and `Brusher` instances for its values.
    The csv file should have the following headers:
    PlaybrushID,TimestampUTC,UpTime,DownTime,LeftTime,RightTime,NoneTime

    Rows that do not have all of the fields filled in are ignored.

    Example:
    PlaybrushID,TimestampUTC,UpTime,DownTime,LeftTime,RightTime,NoneTime,
    PB1000000001,Mon Sep 18 2017 08:03:18 GMT+0100 (BST),6.1,0.2,0.5,0.7,6.7,
    PB1000000001,Mon Sep 18 2017 08:03:18 GMT+0100 (BST),6.1,0.2,0.5,0.7,6.7,
    PB1000000002,Tue Sep 19 2017 08:01:15 GMT+0100 (BST),6.1,0.2,0.5,0.7,6.7,

    :param csv_file_content: The file stream
    :return: A dictionary mapping user ids to their respective `Brusher` instances.
    """
    id_brusher_dict = dict()
    csv_file_content.seek(0)
    csv_reader = csv.DictReader(csv_file_content)
    for row in csv_reader:
        if any((not value for key, value in row.items() if key)):
            continue
        row_playbrush_id = row["PlaybrushID"]

        # Creates a brusher if one does not exist and adds it to the dictionary
        brusher: Brusher = id_brusher_dict.get(row_playbrush_id, Brusher(row_playbrush_id))
        id_brusher_dict[row_playbrush_id] = brusher

        session_timestamp_string = row["TimestampUTC"][:-6]  # Remove the timezone tip
        session = BrushingSession(
            timestamp=datetime.strptime(session_timestamp_string, TIMESTAMP_FORMAT),
            up_seconds=Decimal(row["UpTime"]),
            down_seconds=Decimal(row["DownTime"]),
            left_seconds=Decimal(row["LeftTime"]),
            right_seconds=Decimal(row["RightTime"]),
            other_seconds=Decimal(row["NoneTime"])
        )

        brusher.add_raw_session(session)
    return id_brusher_dict


def collect_group_user_brushing_data(user_brushing_data_id_mapping: Dict[str, Brusher],
                                     group_csv_file_content: TextIO) -> Dict[str, List[Brusher]]:
    """
    Assigns the users to groups based on the file describing the groups. The file should be a csv that has two headers:
    group, PBID. Being respectively the group id and user id.

    Example csv file:
    group,PBID
    A,PB0000000000
    B,PB0000000001

    :param user_brushing_data_id_mapping: A dictionary with the user ids for keys and the
     `Brusher` instance representing that user for the value.
    :param group_csv_file_content: The csv file content with the group assignments.
    :return:
    """
    group_brushing_data: Dict[str, List[Brusher]] = defaultdict(list)
    group_csv_file_content.seek(0)
    csv_reader = csv.DictReader(group_csv_file_content)
    for row in csv_reader:
        group, playbrush_id = row["group"], row["PBID"]
        if playbrush_id in user_brushing_data_id_mapping:
            group_brushing_data[group].append(user_brushing_data_id_mapping[playbrush_id])
    return group_brushing_data


def obtain_week(day: date) -> List[date]:
    """
    Obtains a list of the 7 dates corresponding to the dates in the week for the date provided.
    The indexes are organized sequentially, Monday being on the 0 index and Sunday being on the 7 index.

    :param day: The day used to extrapolate the week.
    :return: A list of dates in the week for the day provided, including said day.
    """
    date_weekday = day.weekday()
    week = list()
    for i in range(7):
        week.append(day + timedelta(days=i - date_weekday))
    return week


def create_week_user_info_csv(brusher_data: Dict[str, List[Brusher]], output_path: str, day: Optional[date] = None):
    """
    Writes a csv file to `output_path` with information regarding the users' brushing habits in the specified week.
    Includes:
    * How many times the user brushed in the morning, and in the evening for each day of the week
    * How many days in the week a user brushed twice a day.
    * The total number of valid morning and evening brush sessions in the week.
    * The average time spent brushing per valid session in the week.

    :param brusher_data: A dictionary with the group id for its keys and a list of `Brusher` instances for the values.
    :param output_path: The path to use for writing the resulting csv file.
    :param day: A `date` object representing any weekday for the week to be considered.
    :return:
    """
    week = obtain_week(day)
    # Process groups
    with open(output_path, mode='w') as output_file:
        fieldnames = ["group", "PBID", "mon", "tue", "wed", "thu", "fri", "sat", "sun", "total-brushes",
                      "twice-brushes", "avg-brush-time"]
        writer = csv.DictWriter(output_file, fieldnames=fieldnames)
        writer.writeheader()
        for group, individuals in brusher_data.items():
            for brusher in individuals:
                brusher.process()
                writer.writerow({
                    "group": group,
                    "PBID": brusher.playbrush_id,
                    "mon": len(brusher.get_day_sessions(week[0])),
                    "tue": len(brusher.get_day_sessions(week[1])),
                    "wed": len(brusher.get_day_sessions(week[2])),
                    "thu": len(brusher.get_day_sessions(week[3])),
                    "fri": len(brusher.get_day_sessions(week[4])),
                    "sat": len(brusher.get_day_sessions(week[5])),
                    "sun": len(brusher.get_day_sessions(week[6])),
                    "total-brushes": brusher.total_brushes,
                    "twice-brushes": brusher.total_twice_brushes,
                    "avg-brush-time": brusher.average_brushing_time
                })


class GroupBrushingHabits(NamedTuple):
    group_id: str
    total_valid_brushes: int
    average_time_spent_brushing_per_user: Decimal
    average_time_spent_in_brushing_session: Decimal

    def __str__(self):
        report_string = f"Group: {self.group_id}\n" \
                 f"Total brushes: {self.total_valid_brushes}\n" \
                 f"Average time spent brushing per user: {self.average_time_spent_brushing_per_user}\n" \
                 f"Average time spent in every brushing session: {self.average_time_spent_in_brushing_session}"
        return report_string


def extract_group_habits(group_user_brushing_habits_data: Dict[str, List[Brusher]]):
    """
    Takes a dictionary for all the `Brusher` in a group and creates `GroupBrushingHabits` for every single key.
    :param group_user_brushing_habits_data: A dict with group as key and lists of Brusher for values.
    :return:
    """
    group_brushing_habits_list: List[GroupBrushingHabits] = list()
    for group, brushers in group_user_brushing_habits_data.items():
        total_brushes_in_group = sum((brusher.total_brushes for brusher in brushers))
        all_brushes_in_group: List[BrushingSession] = list(chain(*(brusher.all_processed_brushes for brusher in brushers)))
        average_time_user_spent_brushing_per_user = sum((brusher.average_brushing_time for brusher in brushers)) / len(
            brushers)

        total_time_spent_brushing = sum(brush.total_seconds for brush in all_brushes_in_group)
        average_time_spent_in_brushing_session = total_time_spent_brushing / len(all_brushes_in_group)

        group_habits = GroupBrushingHabits(
            group_id=group,
            total_valid_brushes=total_brushes_in_group,
            average_time_spent_brushing_per_user=average_time_user_spent_brushing_per_user,
            average_time_spent_in_brushing_session=average_time_spent_in_brushing_session
        )
        group_brushing_habits_list.append(group_habits)
    return group_brushing_habits_list


if __name__ == '__main__':
    locale.setlocale(locale.LC_TIME, 'en_UK')
    output_data_file_path = "result.csv"

    first_weekday = date(year=2017, month=9, day=18)

    with open("1_rawdata.csv", mode='r') as raw_data_file:
        user_brushing_data = collect_user_brushing_data(raw_data_file)
    with open("2_groups.csv", mode='r') as group_csv_file:
        group_user_brushing_data = collect_group_user_brushing_data(user_brushing_data, group_csv_file)
    create_week_user_info_csv(group_user_brushing_data, "result.csv", day=first_weekday)

    group_brushing_habits = extract_group_habits(group_user_brushing_data)

    group_brushing_habits.sort(key=lambda x: x.total_valid_brushes, reverse=True)
    for i, group_habits in enumerate(group_brushing_habits, start=1):
        print(f"---------------------Rank {i}----------------------\n{group_habits}")
