Brush data extraction

This is an exercise on manipulating data for toothbrushing sessions.
It relies on csv files, provided either to the API specified in app.py
or loaded through the filesystem to the main in brushing.py

A csv file for the raw data on brushing sessions and a csv file for separating users into groups.

Examples:

1_rawdata.csv
```
PlaybrushID,TimestampUTC,UpTime,DownTime,LeftTime,RightTime,NoneTime,
2500017115,Mon Sep 18 2017 08:03:18 GMT+0100 (BST),6.2,2.1,5.3,1.3,7.7,
```

2_groups.csv
```
group,PBID
D,2500017115
```
This exercise was done with python 3.8 using:
* fastapi
* jinja2
* aiofiles
* uvicorn
* gunicorn
* python-multipart


In brushing.py is all the behaviour for extracting information from the text stream for the csv.
In app.py there are endpoints to use said behaviour to add csv files to a fleeting record that can be consulted.

The API currently only provides behaviour for the user data. Group data can only be obtained locally.

You may run the application by having the two csv files in the project root and
running `python brushing.py`. This will create a `result.csv` file locally and print out group habits on the console.
The groups are ranked on the number of brushes.

You may run the webserver locally by running `python app.py`. You may access the webapp by accessing 
[http://localhost:8000](http://localhost:8000).
It is very barebones and based on the two templates on the templates/ folder.